import unittest
from mysoundsin import SoundSin
from mysound import Sound

class TestSoundSinClass(unittest.TestCase):

    def setUp(self):
        self.duration = 2.0
        self.frequency = 440
        self.amplitude = 0.5
        self.sound_sin = SoundSin(self.duration, self.frequency, self.amplitude)

    def test_inheritance(self):
        # Prueba que un objeto SoundSin herede las propiedades de la clase Sound
        self.assertEqual(self.sound_sin.duration, self.duration)
        self.assertEqual(len(self.sound_sin.buffer), int(self.sound_sin.samples_second * self.duration))

    def test_sin_creation(self):
        # Prueba que la señal sinusoidal se ha creado correctamente en el objeto SoundSin
        self.assertNotEqual(self.sound_sin.buffer, [0] * len(self.sound_sin.buffer))

    def test_sin_amplitude(self):
        # Prueba que la amplitud de la señal sinusoidal coincide con el valor de amplitud proporcionado
        sample = self.sound_sin.samples_second  # Tomamos una muestra del segundo inicial
        expected_amplitude = self.amplitude * Sound.max_amplitude
        self.assertEqual(self.sound_sin.buffer[sample], expected_amplitude)

if __name__ == '__main__':
    unittest.main()