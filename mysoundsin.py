from mysound import Sound  # Impotar la clase Sound

class SoundSin(Sound):
    def __init__(self, duration, frequency, amplitude):
        """
        Initializes a new instance of the class with the specified duration and a sine wave.

        @param duration: The duration of the sound in seconds
        @param frequency: The frequency of the sine wave in Hz
        @param amplitude: The amplitude of the sine wave
        @returns: None
        """
        super().__init__(duration)  # Llama al constructor de la clase base
        self.sin(frequency, amplitude)  # Genera la señal sinusoidal al crear el objeto

# Ejemplo de uso:
duration = 2.0
frequency = 440  # Frecuencia A4
amplitude = 0.5

sound_sin = SoundSin(duration, frequency, amplitude)

# En este punto, sound_sin contiene una señal sinusoidal generada automáticamente.
